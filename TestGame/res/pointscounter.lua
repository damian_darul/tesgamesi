local points = {}

local digitsNumber = 8
local textWidth  = 300
local textHeight = 200

function points:Create( root, scale, x, y )
	
	points["result"] = 0
	points["object"] = root:InsertControl()
	points["object"]:SetPos(x, y)
	points["object"]:SetMatrix(scale, 0, 0, scale)
	points["object"]:AddTree("text.xml", nil, {w = textWidth, h = textHeight, font = "font/normal.xml@linear,mipmap-linear"})
	points["object"]:GetGraphic( "t" ):SetText("00000000")
	
end


function points:AddPoints( pointsVal )
	points["result"] = points["result"] + pointsVal

	local pt = points:GenerateScore(points["result"])
	points["object"]:GetGraphic( "t" ):SetText(pt)
end

function points:SubtractPoints( pointsVal )
	
	points["result"] = points["result"] - pointsVal
	local pt = points:GenerateScore(points["result"])
	points["object"]:GetGraphic( "t" ):SetText(pt)

end

function points:GenerateScore( result )
	
	if result <= 0 then
		return "00000000"
	end
	
	local digits = math.log10(result) + 1 	
	local resultStr = "" .. result
	local loopCtr = digitsNumber - digits
	
	for counter=1,  loopCtr do
		resultStr = "0" .. resultStr
	end

	return resultStr
	
end


return points

