local text = load("text.lua")
local alienShips = {}
local dltMove = 20
local direction = -1
local timeCounter = 0
local animTimeCounter = 1 
local rows = 1
local columns = 8
local pointsCounter = 0 
local shipsImg = {"spaceInvaders_1.png@linear", "spaceInvaders_2.png@linear", "spaceInvaders_3.png@linear"}
local timeout  = 1.5
local leftBorder  = 100
local rightBorder = 1000
local shipImgSize = 50
local points = 15

local shipStatus = 
{ 
    shipOk            = 0,
    shipHit           = 1,
    shipExplodedAnim1 = 2,
    shipExplodedAnim2 = 3,
    shipDestroyed     = 5,
}

function alienShips:Create( root, pointsCtr, level, scale, x, y )    
       
    rows = level
    
    for iterrows = 1, rows do
    
        alienShips[iterrows] = {}
        
        for itercolumns = 1, columns do
        
            alienShips[iterrows][itercolumns] = {}        
            alienShips[iterrows][itercolumns]["x"] = x * itercolumns
            alienShips[iterrows][itercolumns]["y"] = y * iterrows 
            alienShips[iterrows][itercolumns]["points"] = points * iterrows
            alienShips[iterrows][itercolumns]["object"] = root:InsertControl()            
            alienShips[iterrows][itercolumns]["object"]:AddTree( "image_2.xml", nil, {fileName = shipsImg[iterrows]} )
            alienShips[iterrows][itercolumns]["object"]:SetPos( alienShips[iterrows][itercolumns]["x"], alienShips[iterrows][itercolumns]["y"] )
                                                      
            alienShips[iterrows][itercolumns]["state"] = shipStatus.shipOk
            alienShips[iterrows][itercolumns]["dt"] = 0      
        end
    end
    
    alienShips["count"] = rows * columns
    text:Create( root, scale, 450, 250 )
    pointsCounter = pointsCtr

end

function alienShips:Update( dt )
    self:Move(dt)
    self:MakeExplosionAnimation(dt)
end

function alienShips:Move( dt )
    
        timeCounter = timeCounter + dt
        
        if timeCounter > timeout then
        
            local first_r, first_c = alienShips:GetFirstShipIndex()
            local last_r,  last_c  = alienShips:GetLastShipIndex()
            
            if ( alienShips[first_r][first_c] == nil ) or ( alienShips[last_r][last_c] == nil ) then 
                return -1
            end
            
            if alienShips[first_r][first_c]["x"] < leftBorder  then
                direction = 1        
            elseif    alienShips[last_r][last_c]["x"] > rightBorder  then
                direction = -1
            end    

            for iterrows = 1, rows do
            
                for itercolumns = 1, columns do 
                
                    if alienShips[iterrows][itercolumns]["object"] ~= nil then
                    
                        local Offset = dltMove*direction
                        alienShips[iterrows][itercolumns]["x"] = alienShips[iterrows][itercolumns]["x"] + Offset
                        alienShips[iterrows][itercolumns]["object"]:SetPos(alienShips[iterrows][itercolumns]["x"], 
                                                                            alienShips[iterrows][itercolumns]["y"])            
                    end                                              
                end
            end
            timeCounter = 0
        end    
end


function alienShips:GetFirstShipIndex()
    
    local index = {row=1, col=1}
    
    for iterrows = 1, rows do
    
        for itercolumns = 1, columns do 
        
            if alienShips[iterrows][itercolumns]["object"] ~= nil then
            
                if alienShips[iterrows][itercolumns]["x"] < alienShips[index.row][index.col]["x"] then
                
                    index.row = iterrows
                    index.col = itercolumns
                    break
                end                
            end
        end
    end
    
    return index.row, index.col
        
end

function alienShips:GetLastShipIndex()
    
    local index = {row=1, col=1}
    
    for iterrows = 1, rows do 
    
        for itercolumns = columns, 1, -1 do
        
            if alienShips[iterrows][itercolumns]["object"] ~= nil then
            
                if alienShips[iterrows][itercolumns]["x"] > alienShips[index.row][index.col]["x"] then
                
                    index.row = iterrows
                    index.col = itercolumns
                    break
                    
                end    
            end
        end
    end
    
    return index.row, index.col    
end

function alienShips:CheckIfHitTarget( x, y )
    
    for iterrows = 1, rows do    
        
        for itercolumns = 1, columns do
            
            if alienShips[iterrows][itercolumns]["object"] ~= nil then                
                
                if ( alienShips[iterrows][itercolumns]["y"] + shipImgSize >= y ) and ( alienShips[iterrows][itercolumns]["y"] < y ) then
                
                    if ( alienShips[iterrows][itercolumns]["x"] <= x ) and ( alienShips[iterrows][itercolumns]["x"] + shipImgSize > x ) then -- @todo: hardcoded img size to be changed                                        
                        
                        if alienShips[iterrows][itercolumns]["state"] == shipStatus.shipOk  then 
                        
                            pointsCounter:AddPoints( alienShips[iterrows][itercolumns]["points"] )                        
                            alienShips:AddExplosion(iterrows, itercolumns)                                            
                            alienShips["count"] = alienShips["count"] - 1
                            
                        end    
                        
                        if 0 == alienShips["count"] then
                            text:SetText("You Win")
                        end
                        
                        return true        
                    end
                end    
            end    
        end
    end
    
    if( y < 10) then 
        pointsCounter:SubtractPoints(10)
    end
    
    return false
end

function alienShips:AddExplosion( indexRow, indexColumn )
        
    if (alienShips[indexRow][indexColumn]["object"] ~=  nil) and (alienShips[indexRow][indexColumn]["state"] ==  shipStatus.shipDestroyed) then
            
            alienShips[indexRow][indexColumn]["object"]:Remove()
            alienShips[indexRow][indexColumn]["object"] = nil
    
    elseif (alienShips[indexRow][indexColumn]["state"] ~=  shipStatus.shipDestroyed) then 
    
        if alienShips[indexRow][indexColumn]["state"] == shipStatus.shipOk then 
            
            alienShips[indexRow][indexColumn]["object"]:SetSubTree("explosion_1")
            alienShips[indexRow][indexColumn]["state"] = shipStatus.shipHit
        
        elseif alienShips[indexRow][indexColumn]["state"] == shipStatus.shipHit then 
        
            alienShips[indexRow][indexColumn]["object"]:SetSubTree("explosion_2")
            alienShips[indexRow][indexColumn]["state"] = shipStatus.shipExplodedAnim1
            
        elseif alienShips[indexRow][indexColumn]["state"] == shipStatus.shipExplodedAnim1 then
        
            alienShips[indexRow][indexColumn]["object"]:SetSubTree("explosion_1")
            alienShips[indexRow][indexColumn]["state"] = shipStatus.shipExplodedAnim2
            
        elseif alienShips[indexRow][indexColumn]["state"] == shipStatus.shipExplodedAnim2 then    
        
            alienShips[indexRow][indexColumn]["object"]:SetSubTree("explosion_2")
            alienShips[indexRow][indexColumn]["state"] = shipStatus.shipDestroyed
            
        end    
    end    
end

function alienShips:MakeExplosionAnimation( dt )
    
    for iterrows = 1, rows do    
        
        for itercolumns = 1, columns do
            
            if (alienShips[iterrows][itercolumns]["state"] ~= shipStatus.shipOk) and (alienShips[iterrows][itercolumns]["object"] ~= nil) then                
                
                alienShips[iterrows][itercolumns]["dt"] = alienShips[iterrows][itercolumns]["dt"] + dt                
                
                if alienShips[iterrows][itercolumns]["dt"] > 0.5 then                    
                    
                    alienShips:AddExplosion(iterrows, itercolumns)                
                
                    alienShips[iterrows][itercolumns]["dt"] = 0                    
                
                end            
            end            
        end
    end
end



return alienShips