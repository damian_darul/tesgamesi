local text = {}
local digitNum = 8
local width = 300
local height = 100


function text:Create( root, scale, x, y )

	text["object"] = root:InsertControl()
	text["object"]:SetPos(x, y)
	text["object"]:SetMatrix(scale*2.5, 0, 0, scale*2.5)
	text["object"]:AddTree("text.xml", nil, {w = width, h = height, font = "font/normal.xml@linear,mipmap-linear"})	

end

function text:SetText( msg )
	text["object"]:GetGraphic( "t" ):SetText( msg )	
end

return text