#ifndef __APPLICATION_HPP__
#define __APPLICATION_HPP__

#include "claw/application/Application.hpp"
#include <memory>
namespace Guif { class Screen; }

namespace TestGame
{
	

    class TestGameApplication : public Claw::Application
    {
    public:
        TestGameApplication();
        ~TestGameApplication();

        void OnStartup() override;
        void OnShutdown() override;

        void OnUpdate( float dt ) override;
        void OnRender( Claw::Surface* target ) override;
        void OnKeyPress( Claw::KeyCode code ) override;
        void OnKeyRelease( Claw::KeyCode code ) override;
        void OnTouchDown( int x, int y, int button ) override;
        void OnTouchUp( int x, int y, int button ) override;
        void OnTouchMove( int x, int y, int button ) override;

	protected:
		std::unique_ptr<Guif::Screen> m_mainWindow;

    };

}

#endif
